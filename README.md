# Shell commands #

* shield

# Features #

* Basic defense daemons with evasive abalities.
* Decentralized anti-DDoS barrier.
* 2-way learning barrier.
* Management & annonce of REST-able resources cache/env with NoSQL backend.

# List of packages #

### Log Analysis : ###

* denyhosts
* fail2ban

### Traffic Analysis : ###

* psad

### File Analysis : ###

* clamav
* chkrootkit
* rkhunter